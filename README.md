barcodenumber
=============

Python module to validate product codes.

Codes
-----

* ean13

Nutshell
--------

Here a simple example to validate product codes::

    >>> import barcodenumber
    >>> barcodenumber.check_code('ean13','9788478290222')
    True
    >>> barcodenumber.barcodes()
    ['CODE39', 'EAN', 'EAN13', 'EAN8', 'GS1', 'GTIN', 'ISBN', 'ISBN10', 'ISBN13',
    'ISSN', 'JAN', 'PZN', 'UPC', 'UPCA']
